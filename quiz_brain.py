from question_model import Question


class QuizBrain:

    def __init__(self, q_list: [Question]):
        self.q_num = 0
        self.q_list = q_list
        self.user_score = 0

    def still_has_question(self):
        return self.q_num < len(self.q_list)

    def next_question(self):
        current_question: Question = self.q_list[self.q_num]

        self.q_num += 1
        user_input = input(f"Q.{self.q_num}: {current_question.question} (True / False)? ")

        self.check_answer(user_input, current_question.answer)

    def check_answer(self, usr_answer, curr_answer):
        usr_ans = usr_answer[0]
        curr_ans = curr_answer[0]

        if usr_ans.lower() == curr_ans.lower():
            print("You got it right")

            self.user_score += 1

        else:
            print("That's wrong")

        print(f"The correct answer is {curr_answer}")
        print(f"Current score is {self.user_score}/{self.q_num}", end="\n\n")
