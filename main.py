from question_model import Question
from quiz_brain import QuizBrain
import data

if __name__ == "__main__":
    question_bank = []
    q_data = data.question_data

    for q in q_data:
        question_bank.append(Question(q["question"], q["correct_answer"], q["incorrect_answers"][0]))

    quiz_brain = QuizBrain(question_bank)

    while quiz_brain.still_has_question():
        quiz_brain.next_question()

    print("You've completed the challenge.")
    print(f"Your final score was {quiz_brain.user_score}/{quiz_brain.q_num}")
